<?php

namespace Popschool;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class Hello
{
    public function action(Request $request, Application $app, $name)
    {
	    return $app['twig']->render('hello.html.twig', [
	        'name' => $name,
	    ]);
	}
}
